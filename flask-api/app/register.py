from app.auth import auth
from app.api.private import private
from app.api.public import public


def register_blueprints(app):
    app.register_blueprint(auth)
    app.register_blueprint(private)
    app.register_blueprint(public)
