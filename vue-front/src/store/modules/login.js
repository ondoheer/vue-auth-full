import router from "../../router";
import { routesList, API_URL } from "../../config";

/* State */
const state = {
  email: "",
  password: "",
  isLoading: false
};

// Mutations
const mutations = {
  updateLoginEmail(state, email) {
    state.email = email;
  },
  updateLoginPassword(state, password) {
    state.password = password;
  },
  toggleIsLoading(state) {
    state.isLoading = !state.isLoading;
  }
};

// actions
const actions = {
  login({ commit, state }) {
    const params = {
      headers: new Headers({ "Content-Type": "application/json" }),
      method: "POST",
      body: JSON.stringify({
        email: state.email,
        password: state.password
      })
    };
    commit("toggleIsLoading");
    return fetch(`${API_URL}${routesList.loginRoute}`, params)
      .then(res => {
        if (!res.ok) {
          res
            .json()
            .then(body => console.log(body))
            .catch(error => {
              throw new Error(error);
            });
        }
        return res.json();
      })
      .then(json => {
        commit("setTokens", json, { root: true });
        commit("loginSuccess", json, { root: true });
        commit("toggleIsLoading");
        router.push(routesList.private);
      });
  }
};

// getters
const getters = {};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
