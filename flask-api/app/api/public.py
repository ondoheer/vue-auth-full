from flask import Blueprint, jsonify, abort, request


from app.data import list_of_characters

public = Blueprint("public", __name__)


@public.route("/public", methods=["GET"])
def public_chars():
    return jsonify(list(filter(lambda character: character["public"] == True, list_of_characters)))
