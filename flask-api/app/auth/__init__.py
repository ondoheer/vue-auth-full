from flask import Blueprint, jsonify, request

from flask_jwt_extended import (
    create_access_token,
    create_refresh_token,
    jwt_refresh_token_required,
    get_jwt_identity
)


from app.extensions import bcrypt

auth = Blueprint("auth", __name__)


@auth.route('/validate')
@jwt_refresh_token_required
def validate():
    return jsonify({'msg': 'valid token'}), 200


@auth.route('/refresh', methods=['POST'])
@jwt_refresh_token_required
def refresh():
    current_user = get_jwt_identity()
    ret = {
        "access_token": create_access_token(identity=current_user)
    }
    return jsonify(ret), 200


@auth.route('/login', methods=["POST"])
def login():
    if not request.is_json:
        return jsonify({"msg": "Missing JSON in Request"}), 400

    params = request.get_json()

    email = params.get('email', None)
    password = params.get('password', None)

    if not email:
        return jsonify({"msg": "Email Missing"}), 400

    if not password:
        return jsonify({"msg": "Missing password"}), 400

    # validate user logic
    # the identity is the one that will be returned by get_jwt_identity

    # should_login = User.validate_password(email, password)

    should_login = (email == "ondoheer@gmail.com" and password == "s3b40th1!")

    if not should_login:
        return jsonify({'msg': 'invalid email or password'}), 400

    token = {
        "access_token": create_access_token(identity=email),
        "refresh_token": create_refresh_token(identity=email)
    }
    return jsonify(token), 200


@auth.route('/register', methods=["POST"])
def register():
    if not request.is_json:
        return jsonify({"msg": "Missing JSON in Request"}), 400

    params = request.get_json()

    email = params.get('email', None)
    password = params.get('password', None)

    # validate fields
    if not email:
        return jsonify({"msg": "Email Missing"}), 400

    if not password:
        return jsonify({"msg": "Password required"}), 400

    # now we process the registration

    token = {
        "access_token": create_access_token(identity=email),
        "refresh_token": create_refresh_token(identity=email)
    }

    return jsonify(token), 201
