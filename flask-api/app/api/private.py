from flask import Blueprint, jsonify, abort, request
from flask_jwt_extended import jwt_required, get_jwt_identity


from app.data import list_of_characters

private = Blueprint("private", __name__)


@private.route("/private", methods=["GET"])
@jwt_required
def private_chars():
    return jsonify(list(filter(lambda character: character["public"] == False, list_of_characters)))
