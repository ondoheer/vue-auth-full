const state = {
  email: "",
  password: "",
  confirm: ""
};

const mutations = {
  updateRegisterEmail(state, email) {
    state.email = email;
  },
  updateRegisterPassword(state, password) {
    state.password = password;
  },

  updateRegisterConfirm(state, password) {
    state.confirm = password;
  }
};

const actions = {};
const getters = {};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
};
