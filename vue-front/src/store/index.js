import Vue from "vue";
import Vuex from "vuex";
import jwt_decode from "jwt-decode";

import router from "../router";
import register from "./modules/register";
import login from "./modules/login";
import { API_URL, routesList } from "../config";
Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    login,
    register
  },
  state: {
    isLoggedIn: !!localStorage.getItem("access_token"), //double negation muates null to false
    jwt: {
      refresh_token: localStorage.getItem("refresh_token"),
      access_token: localStorage.getItem("access_token")
    }
  },
  mutations: {
    setTokens(state, newTokens) {
      localStorage.setItem("access_token", newTokens["access_token"]);
      localStorage.setItem("refresh_token", newTokens["refresh_token"]);
      state.jwt = newTokens;
    },
    updateAccessToken(state, newToken) {
      localStorage.setItem("access_token", newToken.access_token);
      state.jwt.access_token = newToken.access_token;
    },
    removeTokens(state) {
      localStorage.removeItem("access_token");
      localStorage.removeItem("refresh_token");
      state.jwt = null;
    },
    loginSuccess(state) {
      state.isLoggedIn = true;
    },
    logoutSuccess(state) {
      state.isLoggedIn = false;
    }
  },
  actions: {
    logout({ commit }) {
      commit("removeTokens");
      commit("logoutSuccess");
      router.push("login");
    },
    refreshAccessToken({ state, commit, dispatch }) {
      const params = {
        headers: new Headers({
          "Content-Type": "application/json",
          Authorization: `Bearer ${state.jwt.refresh_token}`
        }),
        method: "POST"
      };
      return fetch(`${API_URL}${routesList.refreshRoute}`, params)
        .then(res => {
          if (!res.ok) {
            return dispatch("logout");
          }
          return res.json();
        })
        .then(json => {
          commit("updateAccessToken", json);
        })
        .catch(error => {
          console.log(error);
        });
    },
    inspectToken({ state, dispatch }) {
      const access_token = state.jwt.access_token;
      const refresh_token = state.jwt.refresh_token;
      // if there is an access token,
      // access_token lives for 5 mins
      // refresh lives for 30 days (default of the backend plugin)
      // if access token has expired and refresh has not, renew access token
      // else logout
      if (access_token && refresh_token) {
        const decoded = {
          access: jwt_decode(access_token),
          refresh: jwt_decode(refresh_token)
        };

        if (
          decoded.access.exp - Date.now() / 1000 < 0 &&
          Date.now() / 1000 - decoded.refresh.exp > 0
        ) {
          dispatch("refreshAccessToken");
        } else if (Date.now() / 1000 - decoded.refresh.exp > 0) {
          dispatch("logout");
        } else {
          // do nothing
          //console.log("continue normal request");
        }
      }
    }
  },
  getters: {
    // this one is for the router guards
    isLoggedIn: state => {
      return state.isLoggedIn;
    }
  }
});
