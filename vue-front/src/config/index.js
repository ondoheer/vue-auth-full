export const API_URL =
  process.env.NODE_ENV === "production"
    ? "//una.url.real"
    : "http://localhost:5000";

export const routesList = {
  loginRoute: "/login",
  registerRoute: "/regiter",
  refreshRoute: "/refresh",
  private: "/private"
};
