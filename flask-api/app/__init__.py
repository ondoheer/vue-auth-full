from flask import Flask, g
from flask_jwt_extended import get_jwt_identity
import datetime

from app.extensions import init_extensions
from app.register import register_blueprints


def create_app(app=None):
    """
    creates a Flask App.
    :param app: Fask application instance
    :param config: object or location of the object to cofnigure the app
    :return: Flask app instance with extensions, blueprints, configs loaded
    """

    if not app:
        app = Flask(__name__)

    # config (has to happend before inits since some extensions
    # require this values)

    app.config["DEVELOPMENT"] = True
    app.config["DEBUG"] = True
    app.config["SECRET_KEY"] = "mellon"
    app.config["JWT_SECRET_KEY"] = "mellon"
    app.config["BCRYPT_LOG_ROUNDS"] = 12
    app.config["JWT_ACCESS_TOKEN_EXPIRES"] = datetime.timedelta(
        seconds=300)

    init_extensions(app)

    # Blueprints
    register_blueprints(app)

    return app
