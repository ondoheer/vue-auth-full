import Vue from "vue";
import Router from "vue-router";

import store from "./store";

import Login from "./views/Login.vue";
import Register from "./views/Register.vue";
import Public from "./views/Public.vue";
import Private from "./views/Private.vue";

Vue.use(Router);

// Guard for protecting routes
const isNotAuthenticated = (to, from, next) => {
  if (!store.getters.isLoggedIn) {
    next();
    return;
  }
  next("/");
};

// Route for blocking views when is authenticated
const isAuthenticated = (to, from, next) => {
  store.dispatch("inspectToken");

  if (store.getters.isLoggedIn) {
    next();
    return;
  }
  next("/login");
};

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "home",
      component: Public
    },
    {
      path: "/private",
      name: "private",
      component: Private,
      beforeEnter: isAuthenticated
    },
    {
      path: "/login",
      name: "login",
      component: Login,
      beforeEnter: isNotAuthenticated
    },
    {
      path: "/register",
      name: "register",
      component: Register,
      beforeEnter: isNotAuthenticated
    }
  ]
});
